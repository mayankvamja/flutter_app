import 'package:flutter/material.dart';
import 'package:fluttery/generated/l10n.dart';

extension LocalizedBuildContext on BuildContext {
  S get s {
    return S.of(this);
  }

  S get localized {
    return S.of(this);
  }
}
