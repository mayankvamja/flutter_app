import 'package:flutter/material.dart';

class Space {
  static Widget hSpace(double width) {
    return SizedBox(width: width);
  }

  static Widget vSpace(double height) {
    return SizedBox(height: height);
  }

  static Widget fromHV(double width, double height) {
    return SizedBox(width: width, height: height);
  }
}
