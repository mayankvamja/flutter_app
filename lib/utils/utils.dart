import 'package:flutter/material.dart';

class Utils {
  Utils._();

  static List<Color> generateRandomColors(String name) {
    switch (name) {
      case "electronics":
        return const [Color(0xFF00B4DB), Color(0xFF0083B0)];
      case "jewelery":
        return const [Color(0xFF22c1c3), Color(0xFFfdbb2d)];
      case "men's clothing":
        return const [Color(0xFF06beb6), Color(0xFF48b1bf)];
      case "women's clothing":
        return const [Color(0xFF12c2e9), Color(0xFFc471ed), Color(0xFBf64f59)];
      default:
        return const [Color(0xFF9D50BB), Color(0xFF6E48AA)];
    }
  }

  static Icon generateRandomIcon(String name) {
    switch (name) {
      case "electronics":
        return const Icon(Icons.electrical_services, color: Colors.white);
      case "jewelery":
        return const Icon(Icons.stay_current_portrait, color: Colors.white);
      case "men's clothing":
        return const Icon(Icons.male, color: Colors.white);
      case "women's clothing":
        return const Icon(Icons.female, color: Colors.white);
      default:
        return const Icon(Icons.star, color: Colors.white);
    }
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1)}";
  }

  String toCamelCase() {
    return split(" ").map((e) => e.capitalize()).join(" ");
  }
}
