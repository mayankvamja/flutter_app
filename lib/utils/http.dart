import 'dart:convert';
import 'package:fluttery/models/product.dart';
import 'package:http/http.dart' as http;

class ShopRepository {
  const ShopRepository();

  static const ShopRepository shared = ShopRepository();

  static const String _host = "fakestoreapi.com";
  static const String _productsPath = "/products/";

  Future<List<Product>> getAllProducts() async {
    Uri uri = Uri.https(_host, "/products");
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final List parsedProducts = jsonDecode(response.body);
      return parsedProducts
          .map((productMap) => Product.fromJson(productMap))
          .toList();
    }

    // throw proper Error
    throw Exception(["Failed to fetch products data."]);
  }

  Future<Product> getProduct(int productId) async {
    Uri uri = Uri.https(_host, "/products/$productId");
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final Map<String, dynamic> parsedJson = jsonDecode(response.body);
      return Product.fromJson(parsedJson);
    }

    // throw proper Error
    throw Exception(["Failed to fetch products data."]);
  }

  Future<List<String>> getAllCategories() async {
    Uri uri = Uri.https(_host, "/products/categories");
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final List parsedCategories = jsonDecode(response.body);
      return parsedCategories.cast<String>();
    }

    // throw proper Error
    throw Exception(["Failed to fetch product categories."]);
  }

  Future<List<Product>> getCategoryProducts(String category) async {
    Uri uri = Uri.https(_host, "/products/category/$category");
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final List parsedProducts = jsonDecode(response.body);
      return parsedProducts
          .map((productMap) => Product.fromJson(productMap))
          .toList();
    }

    // throw proper Error
    throw Exception(["Failed to fetch products data."]);
  }
}
