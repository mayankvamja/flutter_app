part of 'shop_bloc.dart';

abstract class ShopState extends Equatable {
  final List<Product>? products;
  final List<String>? categories;
  final String? errorMessage;
  final BlocStatus status;

  const ShopState({
    this.status = BlocStatus.initial,
    this.products = const [],
    this.categories = const [],
    this.errorMessage,
  });

  @override
  List<Object?> get props => [products, categories];
}

class ShopInitialState extends ShopState {}

class FetchingData extends ShopState {
  const FetchingData() : super(status: BlocStatus.loading);
}

class FetchingProducts extends ShopState {}

class FetchedProducts extends ShopState {
  const FetchedProducts(List<Product> products)
      : super(status: BlocStatus.success, products: products);
}

class FetchProductsFailed extends ShopState {
  const FetchProductsFailed(String errorMessage)
      : super(status: BlocStatus.failure, errorMessage: errorMessage);
}

class FetchingCategories extends ShopState {}

class FetchedCategories extends ShopState {
  const FetchedCategories(List<String> categories)
      : super(status: BlocStatus.success, categories: categories);
}

class FetchCategoriesFailed extends ShopState {
  const FetchCategoriesFailed(String errorMessage)
      : super(status: BlocStatus.failure, errorMessage: errorMessage);
}

class ShopErrorState extends ShopState {
  final String message;

  const ShopErrorState(this.message);
}

///
class ProductDetailsLoaded extends ShopState {
  final Product product;

  const ProductDetailsLoaded(this.product);
}

class CategoryProductsLoaded extends ShopState {
  final List<Product> categoryProducts;

  const CategoryProductsLoaded(this.categoryProducts);
}
