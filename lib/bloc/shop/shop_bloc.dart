import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/utils/blos_status.dart';
import 'package:fluttery/utils/http.dart';

part 'shop_event.dart';
part 'shop_state.dart';

class ShopBloc extends Bloc<ShopEvent, ShopState> {
  List<Product> products = [];

  ShopBloc() : super(ShopInitialState()) {
    on<FetchProducts>((event, emit) async {
      print("Start Fetching Products");
      emit(FetchingProducts());
      try {
        products = await event.fetchAllProducts();
        emit(FetchedProducts(products));
      } on Exception {
        emit(const FetchProductsFailed("Can't fetch products."));
      }
    });

    on<FetchCategories>((event, emit) async {
      print("Start Fetching Categories");
      emit(FetchingCategories());
      try {
        final categories = await event.fetchAllCategories();
        emit(FetchedCategories(categories));
      } on Exception {
        emit(const FetchCategoriesFailed("Can't fetch categories."));
      }
    });

    /// TODO: ...
    // on<FetchProductDetails>((event, emit) async {
    //   final Product product;
    //   emit(FetchingData());

    //   final index =
    //       products.indexWhere((element) => element.id == event._productId);
    //   if (index == -1) {
    //     try {
    //       product = await event.fetchProduct();
    //       emit(ProductDetailsLoaded(product));
    //     } on Exception {
    //       emit(ShopErrorState("Can't fetch product details."));
    //     }
    //   } else {
    //     product = products.elementAt(index);
    //     emit(ProductDetailsLoaded(product));
    //   }
    // });

    on<FetchCategoryProducts>((event, emit) async {
      emit(FetchingData());
      try {
        final products = await event.fetchCategoryProducts();
        emit(CategoryProductsLoaded(products));
      } on Exception {
        emit(ShopErrorState("Can't fetch category products."));
      }
    });
  }
}
