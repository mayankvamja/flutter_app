part of 'shop_bloc.dart';

abstract class ShopEvent extends Equatable {
  const ShopEvent();

  @override
  List<Object> get props => [];
}

class FetchProducts extends ShopEvent {
  final ShopRepository shopRepository;

  const FetchProducts({this.shopRepository = ShopRepository.shared});

  Future<List<Product>> fetchAllProducts() async {
    return await shopRepository.getAllProducts();
  }
}

// class FetchProductDetails extends ShopEvent {
//   final ShopRepository shopRepository;
//   final int _productId;

//   const FetchProductDetails(this._productId,
//       {this.shopRepository = ShopRepository.shared});

//   Future<Product> fetchProduct() async {
//     return shopRepository.getProduct(_productId);
//   }
// }

class FetchCategories extends ShopEvent {
  final ShopRepository shopRepository;

  const FetchCategories({this.shopRepository = ShopRepository.shared});

  Future<List<String>> fetchAllCategories() async {
    return await shopRepository.getAllCategories();
  }
}

class FetchCategoryProducts extends ShopEvent {
  final ShopRepository shopRepository;
  final String category;

  const FetchCategoryProducts(
      {required this.category, this.shopRepository = ShopRepository.shared});

  Future<List<Product>> fetchCategoryProducts() async {
    return await shopRepository.getCategoryProducts(category);
  }
}
