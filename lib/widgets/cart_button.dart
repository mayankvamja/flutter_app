import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttery/router/routes.dart';
import 'package:fluttery/screens/cart/bloc/cart_bloc.dart';

class CartButton extends StatelessWidget {
  const CartButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Stack(
            alignment: AlignmentDirectional.topEnd,
            children: [
              (state.count > 0)
                  ? Container(
                      padding: const EdgeInsets.all(4),
                      decoration: const BoxDecoration(
                        color: Colors.red,
                        shape: BoxShape.circle,
                      ),
                      child: Text(
                        "${state.count}",
                        style:
                            const TextStyle(color: Colors.white, fontSize: 10),
                      ),
                    )
                  : const SizedBox(),
              IconButton(
                  onPressed: () {
                    Navigator.pushNamed(context, Routes.cart);
                  },
                  icon: const Icon(Icons.shopping_cart))
            ],
          ),
        );
      },
    );
  }
}
