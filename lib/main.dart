import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttery/bloc/shop/shop_bloc.dart';
import 'package:fluttery/generated/l10n.dart';
import 'package:fluttery/router/routes.dart';
import 'package:fluttery/screens/cart/bloc/cart_bloc.dart';
import 'package:fluttery/screens/cart/cart_screen.dart';
import 'package:fluttery/screens/home.dart';
import 'package:fluttery/screens/login.dart';
import 'package:fluttery/screens/orders_screen.dart';
import 'package:fluttery/screens/product-details/bloc/product_details_bloc.dart';
import 'package:fluttery/screens/product-details/product_details.dart';
import 'package:fluttery/screens/products_overview/products_overview.dart';
import 'package:fluttery/screens/shop-category/bloc/shop_category_bloc.dart';
import 'package:fluttery/screens/shop-category/shop_category_screen.dart';
import 'package:fluttery/screens/shop-home/bloc/shop_home_bloc.dart';
import 'package:fluttery/screens/shop-home/shop_home_screen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fluttery/theme/app_theme.dart';

void main() {
  runApp(const FlutterApp());
}

class FlutterApp extends StatelessWidget {
  const FlutterApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => ShopBloc()),
        BlocProvider(create: (_) => ShopHomeBloc()),
        BlocProvider(create: (_) => ShopCategoryBloc()),
        BlocProvider(create: (_) => ProductDetailsBloc()),
        BlocProvider(create: (_) => CartBloc()),
      ],
      child: const MainApp(),
    );
  }
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _shopHomeBLoC = BlocProvider.of<ShopHomeBloc>(context);
    final _shopCategoryBLoC = BlocProvider.of<ShopCategoryBloc>(context);
    final _productDetailsBLoC = BlocProvider.of<ProductDetailsBloc>(context);
    final _cartBLoC = BlocProvider.of<CartBloc>(context);

    return MaterialApp(
      title: 'Fluttery',
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''), // English, no country code
        Locale('es', ''), // Spanish, no country code
      ],
      theme: AppTheme.light,
      initialRoute: "shop",
      routes: {
        Routes.home: (_) => const HomePage(),
        Routes.login: (_) => const Login(),
        // Routes.shop: (_) => const ProductsOverview(),
        Routes.shop: (_) => ShopHomeScreen(bloc: _shopHomeBLoC),
        Routes.productsOverview: (_) => ProductsOverview(),
        Routes.category: (_) => ShopCategoryScreen(bloc: _shopCategoryBLoC),
        Routes.productDetail: (_) => ProductDetail(bloc: _productDetailsBLoC),
        Routes.cart: (_) => CartScreen(bloc: _cartBLoC),
        Routes.orders: (_) => const OrdersScreen(),
      },
    );
  }
}
