import 'package:flutter/material.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/utils/http.dart';

class Products with ChangeNotifier {
  final List<Product> _items = [];

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favoriteItems {
    return items.where((element) => element.isFavorite).toList();
  }

  Product findById(int id) {
    return _items.firstWhere((element) => element.id == id);
  }

  void fetchAllProducts() async {
    _items.addAll(await ShopRepository.shared.getAllProducts());
    notifyListeners();
  }
}
