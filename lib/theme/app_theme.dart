import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData light = ThemeData(
    brightness: Brightness.light,
    primaryColor: const Color(0xFF0081a7),
    colorScheme: const ColorScheme(
      primary: Color(0xFF0081a7),
      primaryVariant: Color(0xFF272640),
      secondary: Color(0xFF00afb9),
      secondaryVariant: Color(0xFF3e1f47),
      surface: Color(0xFFFFFFFF),
      background: Color(0xFFedf2f4),
      error: Color(0xFFf94144),
      onPrimary: Color(0xFFfdfcdc),
      onSecondary: Color(0xFFfed9b7),
      onSurface: Color(0xFF003049),
      onBackground: Color(0xFF2b2d42),
      onError: Color(0xFFfdfcdc),
      brightness: Brightness.light,
    ),

    // Define the default font family.
    /*fontFamily: 'Gilroy',*/

    // Define the default `TextTheme`. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    // textTheme: const TextTheme(
    //   headline1: TextStyle(fontSize: 68.0, fontWeight: FontWeight.bold),
    //   headline6: TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
    //   bodyText2: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
    // ),
  );
}
