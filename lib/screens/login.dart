import 'package:flutter/material.dart';
import 'package:fluttery/screens/home.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  String? _email;
  String? _password;

  void _login() {
    if (_loginFormKey.currentState!.validate()) {
      _loginFormKey.currentState?.save();
      if (_email == "abcd@gmail.com" && _password == "Test@123") {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text("Login successful.")),
        );
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const HomePage()),
        );
      }
    }
  }

  String? _emailValidator(String? value) {
    if (value != null && value.isNotEmpty) {
      RegExp emailRegex = RegExp(
        r"""^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$""",
        caseSensitive: false,
        multiLine: false,
      );
      if (!emailRegex.hasMatch(value)) {
        return "Please enter valid email address";
      }
      return null;
    }
    return "Please enter yout email address";
  }

  String? _passwordValidator(String? value) {
    if (value != null && value.isNotEmpty) {
      RegExp emailRegex = RegExp(
        r"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$",
        caseSensitive: false,
        multiLine: false,
      );
      if (!emailRegex.hasMatch(value)) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
              content: Text(
                  "Password must be minimum eight characters, at least one letter, one number and one special character.")),
        );
        return "Please enter valid password";
      }
      return null;
    }
    return "Please enter your password";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("App"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _loginFormKey,
            child: Column(
              children: [
                const Image(
                  height: 150,
                  fit: BoxFit.contain,
                  image: AssetImage("resources/images/authentication.png"),
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: "Email",
                    hintText: "xxxxxxxx@xxxx.xxx",
                    icon: Icon(Icons.email_outlined),
                  ),
                  onSaved: (String? value) {
                    _email = value;
                  },
                  validator: _emailValidator,
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: "Password",
                    hintText: "********",
                    icon: Icon(Icons.password),
                  ),
                  obscureText: true,
                  onSaved: (String? value) {
                    _password = value;
                  },
                  validator: _passwordValidator,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: ElevatedButton(
                    onPressed: _login,
                    child: const Text("Login"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: ElevatedButton(
                    onPressed: () {
                      ScaffoldMessenger.of(this.context).showSnackBar(
                        const SnackBar(content: Text("Login successful.")),
                      );
                    },
                    child: const Text("Login"),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
