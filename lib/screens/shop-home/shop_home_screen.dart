import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttery/router/routes.dart';
import 'package:fluttery/screens/shop-home/bloc/shop_home_bloc.dart';
import 'package:fluttery/screens/shop-home/ui/categories_list.dart';
import 'package:fluttery/screens/shop-home/ui/error_box.dart';
import 'package:fluttery/screens/shop-home/ui/products_grid.dart';
import 'package:fluttery/utils/spacer.dart';
import 'package:fluttery/widgets/cart_button.dart';

class ShopHomeScreen extends StatelessWidget {
  const ShopHomeScreen({required ShopHomeBloc bloc, Key? key})
      : _bloc = bloc,
        super(key: key);

  final ShopHomeBloc _bloc;

  Widget _categoriesGrid() {
    // return BlocBuilder<ShopHomeBloc, ShopHomeState>(
    //   builder: (BuildContext context, ShopHomeState state) {
        Widget content = const Center(
            child: Padding(
          padding: EdgeInsets.all(20.0),
          child: CircularProgressIndicator(),
        ));

        final error = _bloc.state.fetchCategoriesError;
        final categories = _bloc.state.categories;
        if (error != null) {
          content = ErrorBox(errorMessage: error);
        } else if (categories != null) {
          content = CategoriesList(categories: categories);
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 0, 10),
              child: Text(
                "Product Categories",
                style: TextStyle(fontSize: 20, color: Color(0xFF212121)),
              ),
            ),
            content,
          ],
        );
      // },
    // );
  }

  Widget _productsGrid() {
    return BlocBuilder<ShopHomeBloc, ShopHomeState>(
      builder: (BuildContext context, ShopHomeState state) {
        List<Widget> contents = [
          const Center(
              child: Padding(
            padding: EdgeInsets.all(50.0),
            child: CircularProgressIndicator(),
          ))
        ];

        final error = state.fetchProductsError;
        final products = state.products;
        if (error != null) {
          contents = [ErrorBox(errorMessage: error)];
        } else if (products != null) {
          contents = [
            ProductsGrid(scrollable: false, products: products.sublist(0, 4)),
            const SizedBox(height: 4),
            Container(
              color: Theme.of(context).colorScheme.surface,
              child: Row(
                children: [
                  const Spacer(),
                  TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, Routes.productsOverview,
                            arguments: products);
                      },
                      child: Row(
                        children: const [
                          Text("See all"),
                          Icon(Icons.arrow_right_sharp),
                        ],
                      )),
                  const SizedBox(width: 20)
                ],
              ),
            )
          ];
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(10, 20, 0, 20),
              child: Text(
                "New Launches",
                style: TextStyle(fontSize: 20, color: Color(0xFF212121)),
              ),
            ),
            ...contents,
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Completer? _homePageLoadingCompleter;
    _load();

    return Scaffold(
      appBar: AppBar(
        title: const Text("AShop"),
        actions: const [
          CartButton(),
        ],
      ),
      backgroundColor: Theme.of(context).colorScheme.background,
      body: SafeArea(
        child: BlocListener<ShopHomeBloc, ShopHomeState>(
          listener: (context, state) {
            if (!state.isLoading) {
              _homePageLoadingCompleter?.complete(0);
            }
          },
          child: RefreshIndicator(
            onRefresh: () async {
              _homePageLoadingCompleter = Completer();
              _load(reload: true);
              await _homePageLoadingCompleter!.future;
              _homePageLoadingCompleter = null;
            },
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _categoriesGrid(),
                  Space.vSpace(10),
                  const Divider(height: 2, color: Colors.grey),
                  Space.vSpace(10),
                  _productsGrid(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _load({bool reload = false}) {
    if (reload ||
        _bloc.state.categories == null ||
        _bloc.state.products == null) {
      _bloc.add(const FetchShopHomePageData());
    }
  }

  /*
  void _loadCategories() {
    _bloc.add(const FetchShopCategories(repository: ShopRepository.shared));
  }

  void _loadProducts() {
    _bloc.add(const FetchShopProducts(repository: ShopRepository.shared));
  }*/
}
