import 'package:flutter/material.dart';

class ProductPriceText extends StatelessWidget {
  const ProductPriceText({
    Key? key,
    required this.priceText,
    this.mrpText = "",
    this.discountText = "",
    double fontSize = 18,
    double? mrpFontSize,
    double? discountFontSize,
  })  : priceFontSize = fontSize,
        mrpFontSize = mrpFontSize ?? (fontSize - 2),
        discountFontSize = discountFontSize ?? (fontSize - 2),
        super(key: key);

  final String priceText, mrpText, discountText;
  final double priceFontSize, mrpFontSize, discountFontSize;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "",
        children: <TextSpan>[
          TextSpan(
            text: priceText,
            style: TextStyle(
              fontSize: priceFontSize,
              color: Colors.black,
              fontWeight: FontWeight.w700,
            ),
          ),
          TextSpan(
            text: mrpText,
            style: TextStyle(
              fontSize: mrpFontSize,
              color: Colors.grey,
              decoration: TextDecoration.lineThrough,
            ),
          ),
          TextSpan(
            text: discountText,
            style: TextStyle(
              fontSize: discountFontSize,
              color: Colors.teal,
            ),
          ),
        ],
      ),
    );
  }
}
