import 'package:flutter/material.dart';
import 'package:fluttery/router/routes.dart';
import 'package:fluttery/utils/spacer.dart';
import 'package:fluttery/utils/utils.dart';

class CategoriesList extends StatelessWidget {
  final List<String> categories;
  const CategoriesList({required this.categories, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 75,
      child: ListView.separated(
        padding: const EdgeInsets.all(8),
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (context, index) =>
            CategoryListItem(categories.elementAt(index)),
        separatorBuilder: (context, index) => const SizedBox(width: 15),
        // ),
      ),
    );
  }
}

class CategoryListItem extends StatelessWidget {
  final String category;
  const CategoryListItem(
    this.category, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        gradient: LinearGradient(colors: Utils.generateRandomColors(category)),
      ),
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, Routes.category, arguments: category);
        },
        child: Row(
          children: [
            Utils.generateRandomIcon(category),
            Space.hSpace(10),
            Text(
              category.toCamelCase(),
              style: const TextStyle(fontSize: 18, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
