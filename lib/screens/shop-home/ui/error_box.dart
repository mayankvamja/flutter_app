import 'package:flutter/material.dart';

class ErrorBox extends StatelessWidget {
  final void Function()? onRefresh;
  final String? refreshButtonText;
  final String errorMessage;
  const ErrorBox(
      {required this.errorMessage,
      this.onRefresh,
      this.refreshButtonText,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(errorMessage),
          const SizedBox(height: 20),
          (onRefresh != null)
              ? ElevatedButton(
                  onPressed: onRefresh,
                  child: Text(refreshButtonText ?? "Retryu"),
                )
              : const SizedBox()
        ],
      ),
    );
  }
}
