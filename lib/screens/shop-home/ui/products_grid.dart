import 'package:flutter/material.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/router/routes.dart';
import 'package:fluttery/screens/shop-home/ui/product_price_text.dart';

class ProductsGrid extends StatelessWidget {
  final List<Product> products;
  final bool? scrollable;
  const ProductsGrid({this.scrollable = true, required this.products, Key? key})
      : super(key: key);

  static int axisCount(double width) {
    if (width <= 500) {
      // 2 parts
      return 2;
    } else if (width > 500 && width <= 950) {
      // 3 parts
      return 3;
    } else {
      // 4 parts
      return 4;
    }
  }

  SliverGridDelegateWithFixedCrossAxisCount productGridDelegate(
      BuildContext context) {
    final _fullWidth = MediaQuery.of(context).size.width;
    final _axisCount = axisCount(_fullWidth);
    final _width = _fullWidth / _axisCount;
    final _height = _width + 90;
    final _aspectRatio = _width / _height;
    return SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: _axisCount,
      childAspectRatio: _aspectRatio,
      crossAxisSpacing: 2,
      mainAxisSpacing: 2,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics:
          (scrollable ?? true) ? null : const NeverScrollableScrollPhysics(),
      itemCount: products.length,
      gridDelegate: productGridDelegate(context),
      itemBuilder: (context, index) => ProductItem(products.elementAt(index)),
    );
  }
}

class ProductItem extends StatelessWidget {
  final Product product;
  const ProductItem(this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.productDetail,
            arguments: product.id);
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        color: Colors.white,
        child: Column(
          children: [
            Expanded(
              child: Image.network(product.image, fit: BoxFit.contain),
            ),
            const SizedBox(height: 10),
            Text(
              product.title,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: const TextStyle(fontSize: 14),
            ),
            ProductPriceText(
              priceText: "₹${(product.price.price).toStringAsFixed(2)} ",
              mrpText: (product.price.mrp).toStringAsFixed(2),
              discountText: " ${product.price.discount}% off",
              fontSize: 14,
            ),
          ],
        ),
      ),
    );
  }
}
