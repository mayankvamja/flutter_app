import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/utils/http.dart';

part 'shop_home_event.dart';
part 'shop_home_state.dart';

class ShopHomeBloc extends Bloc<ShopHomeEvent, ShopHomeState> {
  ShopHomeBloc() : super(const ShopHomeState()) {
    on<FetchShopHomePageData>((event, emit) async {
      emit(state.copyWith(loading: true));
      try {
        List responses = await Future.wait([
          event.fetchProducts(),
          event.fetchCategories(),
        ]);
        emit(state.copyWith(
          loading: false,
          products: responses[0] as List<Product>,
          categories: responses[1] as List<String>,
        ));
      } on Exception {
        emit(state.copyWith(
          loading: false,
          fetchProductsError: "Something went wrong...",
        ));
      }
    });

    on<FetchShopProducts>((event, emit) async {
      emit(const ShopHomeState(isLoading: true));
      try {
        final List<Product>? _products = await event.fetchProducts();
        emit(state.copyWith(
          loading: false,
          products: _products,
        ));
      } on Exception {
        emit(state.copyWith(
          loading: false,
          fetchProductsError: "Something went wrong...",
        ));
      }
    });

    on<FetchShopCategories>((event, emit) async {
      emit(state.copyWith(loading: true));
      try {
        final List<String>? _categories = await event.fetchCategories();
        emit(state.copyWith(
          loading: false,
          categories: _categories,
          fetchCategoriesError: null,
        ));
      } on Exception {
        emit(state.copyWith(
          loading: false,
          fetchCategoriesError: "Something went wrong...",
        ));
      }
    });
  }
}
