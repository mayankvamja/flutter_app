part of 'shop_home_bloc.dart';

abstract class ShopHomeEvent extends Equatable {
  final ShopRepository repository;
  const ShopHomeEvent({required this.repository});

  @override
  List<Object> get props => [];
}

class FetchShopHomePageData extends ShopHomeEvent {
  const FetchShopHomePageData(
      {ShopRepository repository = ShopRepository.shared})
      : super(repository: repository);

  Future<List<String>> fetchCategories() async {
    return await repository.getAllCategories();
  }

  Future<List<Product>> fetchProducts() async {
    return await repository.getAllProducts();
  }
}

class FetchShopCategories extends ShopHomeEvent {
  const FetchShopCategories({ShopRepository repository = ShopRepository.shared})
      : super(repository: repository);

  Future<List<String>> fetchCategories() async {
    return await repository.getAllCategories();
  }
}

class FetchShopProducts extends ShopHomeEvent {
  const FetchShopProducts({ShopRepository repository = ShopRepository.shared})
      : super(repository: repository);

  Future<List<Product>> fetchProducts() async {
    return await repository.getAllProducts();
  }
}
