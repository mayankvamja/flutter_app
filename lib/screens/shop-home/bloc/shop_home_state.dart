part of 'shop_home_bloc.dart';

class ShopHomeState extends Equatable {
  final bool isLoading;
  final List<String>? categories;
  final List<Product>? products;
  final String? fetchCategoriesError;
  final String? fetchProductsError;

  const ShopHomeState({
    this.isLoading = false,
    this.categories,
    this.products,
    this.fetchCategoriesError,
    this.fetchProductsError,
  });

  ShopHomeState copyWith({
    required bool loading,
    List<String>? categories,
    List<Product>? products,
    String? fetchCategoriesError,
    String? fetchProductsError,
  }) {
    return ShopHomeState(
      isLoading: loading,
      categories: categories ?? this.categories,
      products: products ?? this.products,
      fetchCategoriesError: fetchCategoriesError,
      fetchProductsError: fetchProductsError,
    );
  }

  @override
  List<Object?> get props => [products, categories, isLoading];
}
