part of 'product_details_bloc.dart';

abstract class ProductDetailsEvent extends Equatable {
  final ShopRepository repository;
  const ProductDetailsEvent({required this.repository});

  @override
  List<Object> get props => [];
}

class FetchProductDetails extends ProductDetailsEvent {
  final int productId;
  const FetchProductDetails({
    required this.productId,
    ShopRepository repository = ShopRepository.shared,
  }) : super(repository: repository);

  Future<Product> fetchProductDetail() async {
    return await repository.getProduct(productId);
  }
}

class RefetchProductDetails extends ProductDetailsEvent {
  final int productId;
  const RefetchProductDetails({
    required this.productId,
    ShopRepository repository = ShopRepository.shared,
  }) : super(repository: repository);

  Future<Product> refetchProductDetail() async {
    return await repository.getProduct(productId);
  }
}
