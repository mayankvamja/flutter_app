part of 'product_details_bloc.dart';

class ProductDetailsState extends Equatable {
  // final BlocStatus status;
  final String? error;
  final bool isLoading;
  final Product? product;
  const ProductDetailsState({
    // this.status = BlocStatus.initial,
    this.isLoading = false,
    this.product,
    this.error,
  });

  ProductDetailsState copyWith({
    // BlocStatus status = BlocStatus.initial,
    bool loading = false,
    Product? product,
    String? error,
  }) {
    return ProductDetailsState(
      // status: status,
      isLoading: loading,
      product: product ?? this.product,
      error: error,
    );
  }

  @override
  List<Object?> get props => [product];
}
