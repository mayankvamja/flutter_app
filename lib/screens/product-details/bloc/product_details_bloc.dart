import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fluttery/bloc/shop/shop_bloc.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/utils/blos_status.dart';
import 'package:fluttery/utils/http.dart';

part 'product_details_event.dart';
part 'product_details_state.dart';

class ProductDetailsBloc
    extends Bloc<ProductDetailsEvent, ProductDetailsState> {
  ProductDetailsBloc() : super(const ProductDetailsState()) {
    on<FetchProductDetails>((event, emit) async {
      emit(const ProductDetailsState(isLoading: true));
      try {
        final product = await event.fetchProductDetail();
        emit(state.copyWith(loading: false, product: product));
      } on Exception {
        emit(state.copyWith(loading: false, error: "Something went wrong..."));
      }
    });

    on<RefetchProductDetails>((event, emit) async {
      print("RefetchProductDetails starting");
      emit(state.copyWith(loading: true));
      try {
        final product = await event.refetchProductDetail();
        emit(state.copyWith(loading: false, product: product));
        print("RefetchProductDetails success");
      } on Exception {
        emit(state.copyWith(loading: false, error: "Something went wrong..."));
        print("RefetchProductDetails failure");
      }
    });
  }
}
