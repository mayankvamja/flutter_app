import 'package:flutter/material.dart';

class ProductQuantityWidget extends StatelessWidget {
  const ProductQuantityWidget({
    required this.quantity,
    required this.add,
    required this.remove,
    Key? key,
  }) : super(key: key);

  final int quantity;
  final void Function()? add;
  final void Function()? remove;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      decoration: const BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Row(
        children: [
          InkWell(
            onTap: remove,
            child: Icon(Icons.remove,
                color: quantity > 1 ? Colors.blue : Colors.grey),
          ),
          Text("  $quantity  "),
          InkWell(
            onTap: add,
            child: const Icon(Icons.add, color: Colors.blue),
          ),
        ],
      ),
    );
  }
}
