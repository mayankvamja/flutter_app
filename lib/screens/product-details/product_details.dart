import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/screens/cart/bloc/cart_bloc.dart';
import 'package:fluttery/screens/product-details/bloc/product_details_bloc.dart';
import 'package:fluttery/screens/shop-home/ui/error_box.dart';
import 'package:fluttery/screens/shop-home/ui/product_price_text.dart';
import 'package:fluttery/utils/spacer.dart';
import 'package:fluttery/screens/product-details/ui/product_quantity.dart';
import 'package:fluttery/widgets/cart_button.dart';
import 'package:fluttery/widgets/shimmer.dart';

class ProductDetail extends StatelessWidget {
  const ProductDetail({required ProductDetailsBloc bloc, Key? key})
      : _bloc = bloc,
        super(key: key);

  final ProductDetailsBloc _bloc;

  void _reloadProductDetails(int productId) {
    _bloc.add(RefetchProductDetails(productId: productId));
  }

  void _loadProductDetails(int productId) {
    if ((_bloc.state.product?.id != productId)) {
      _bloc.add(FetchProductDetails(productId: productId));
    }
  }

  @override
  Widget build(BuildContext context) {
    final _productId = ModalRoute.of(context)?.settings.arguments as int;
    _loadProductDetails(_productId);
    Completer? _loadingCompleter;

    return Scaffold(
      appBar: AppBar(
        actions: const [CartButton()],
      ),
      body: BlocConsumer<ProductDetailsBloc, ProductDetailsState>(
        listener: (BuildContext context, ProductDetailsState state) {
          if (!state.isLoading) {
            _loadingCompleter?.complete(1);
          }
        },
        builder: (context, state) {
          final product = state.product;
          final error = state.error;

          if (error != null) {
            return ErrorBox(
              errorMessage: error,
              onRefresh: () => _reloadProductDetails(_productId),
            );
          }

          return RefreshIndicator(
            onRefresh: () async {
              if (state.isLoading) {
                return;
              }
              _loadingCompleter = Completer();
              _reloadProductDetails(_productId);
              await _loadingCompleter!.future;
            },
            child: Shimmer(
              child: ShimmerLoading(
                isLoading: state.isLoading,
                child: _ProductDetailsView(product),
              ),
            ),
          );
        },
      ),

      /*floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.redAccent,
        child:
            Icon(product.isFavorite ? Icons.favorite : Icons.favorite_outline),
      ),*/
    );
  }
}

class _ProductDetailsView extends StatefulWidget {
  final Product? product;
  const _ProductDetailsView(this.product, {Key? key}) : super(key: key);

  @override
  __ProductDetailsViewState createState() => __ProductDetailsViewState();
}

class __ProductDetailsViewState extends State<_ProductDetailsView> {
  int _quantity = 1;
  late Product? product;

  // states
  @override
  void initState() {
    super.initState();
    product = widget.product;
  }

  @override
  Widget build(BuildContext context) {
    final cartBloc = BlocProvider.of<CartBloc>(context, listen: false);
    final isLoading = (product == null);

    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ShimmerText(
            isLoading: isLoading,
            lines: 2,
            builder: (context) => Text(
              product!.title,
              style: const TextStyle(
                color: Color(0xFF212121),
                fontSize: 24,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          Space.vSpace(10),
          Container(
            color: isLoading ? Colors.grey : null,
            constraints: const BoxConstraints(
              minHeight: 300,
              minWidth: 500,
              maxHeight: 400,
            ),
            child: product == null
                ? const SizedBox()
                : Image(
                    image: NetworkImage(product!.image),
                    fit: BoxFit.contain,
                    alignment: Alignment.center,
                  ),
          ),
          Space.vSpace(10),
          ShimmerText(
            isLoading: isLoading,
            builder: (context) => ProductPriceText(
              priceText: "₹${(product!.price.price).toStringAsFixed(2)}  ",
              mrpText: (product!.price.mrp).toStringAsFixed(2),
              discountText: "  ${product!.price.discount}% off",
              fontSize: 26,
              mrpFontSize: 22,
              discountFontSize: 22,
            ),
          ),
          Space.vSpace(10),
          Row(
            children: [
              ProductQuantityWidget(
                quantity: _quantity,
                add: () {
                  setState(() {
                    _quantity += 1;
                  });
                },
                remove: () {
                  setState(() {
                    _quantity > 1 ? _quantity -= 1 : null;
                  });
                },
              ),
              Space.hSpace(10),
              Expanded(
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size(0, 45),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10), // <-- Radius
                    ),
                  ),
                  onPressed: () {
                    if (product == null) {
                      return;
                    }
                    cartBloc.add(AddToCartEvent(product: product!));

                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text("Added to cart."),
                      ),
                    );
                  },
                  icon: const Icon(Icons.shopping_cart),
                  label: const Text("Add to cart"),
                ),
              ),
            ],
          ),
          const SizedBox(height: 100),
        ],
      ),
    );
  }
}
