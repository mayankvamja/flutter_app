part of 'cart_bloc.dart';

class CartState extends Equatable {
  final BlocStatus status;
  final List<CartItem> items;
  const CartState({
    this.status = BlocStatus.initial,
    this.items = const [],
  });

  int get count => items.length;

  CartState copyWith({
    required BlocStatus status,
    required List<CartItem> items,
  }) {
    return CartState(status: status, items: items);
  }

  @override
  List<Object> get props => [items];
}
