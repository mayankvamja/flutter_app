part of 'cart_bloc.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();

  @override
  List<Object> get props => [];
}

class AddToCartEvent extends CartEvent {
  final Product product;

  const AddToCartEvent({required this.product});
}

class RemoveFromCartEvent extends CartEvent {
  final CartItem item;

  const RemoveFromCartEvent({required this.item});
}

class IncreaseQuantityEvent extends CartEvent {
  final CartItem item;

  const IncreaseQuantityEvent({required this.item});
}

class DecreaseQuantityEvent extends CartEvent {
  final CartItem item;

  const DecreaseQuantityEvent({required this.item});
}
