import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fluttery/bloc/shop/shop_bloc.dart';
import 'package:fluttery/models/cart.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/utils/blos_status.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(const CartState()) {
    on<AddToCartEvent>((event, emit) {
      print("AddToCartEvent Will Execute");
      // emit(const CartState(status: BlocStatus.loading));
      emit(state.copyWith(
        status: BlocStatus.success,
        items: state.items.addingItem(event.product),
      ));
      print(state.items.toString());
      // emit(const CartState(status: BlocStatus.success));
    });

    on<RemoveFromCartEvent>((event, emit) {
      // emit(const CartState(status: BlocStatus.loading));
      emit(state.copyWith(
        status: BlocStatus.success,
        items: state.items
            .where((element) => element.product.id != event.item.product.id)
            .toList(),
      ));
      // emit(const CartState(status: BlocStatus.success));
    });

    on<IncreaseQuantityEvent>((event, emit) {
      // emit(const CartState(status: BlocStatus.loading));
      emit(state.copyWith(
        status: BlocStatus.success,
        items: state.items.increasingQuantity(event.item),
      ));
      // emit(const CartState(status: BlocStatus.success));
    });

    on<DecreaseQuantityEvent>((event, emit) {
      // emit(const CartState(status: BlocStatus.loading));
      emit(state.copyWith(
        status: BlocStatus.success,
        items: state.items.decreasingQuantity(event.item),
      ));
      // emit(const CartState(status: BlocStatus.success));
    });
  }
}
