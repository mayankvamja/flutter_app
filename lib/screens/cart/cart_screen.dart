import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttery/models/cart.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/screens/cart/bloc/cart_bloc.dart';
import 'package:fluttery/screens/shop-home/ui/product_price_text.dart';
import 'package:fluttery/utils/spacer.dart';
import 'package:fluttery/screens/product-details/ui/product_quantity.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({required CartBloc bloc, Key? key})
      : _bloc = bloc,
        super(key: key);

  final CartBloc _bloc;

  @override
  _CartScreenState createState() {
    return _CartScreenState();
  }
}

class _CartScreenState extends State<CartScreen> {
  _CartScreenState();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Cart"),
      ),
      backgroundColor: const Color(0xFFF0F0F0),
      body: SafeArea(
        child: Stack(
          children: [
            BlocBuilder<CartBloc, CartState>(builder: (context, state) {
              return ListView(
                children: [
                  // List of cart items
                  ...state.items
                      .map((item) => _CartItem(cartItem: item))
                      .toList(),
                  const _CartSummary(),
                  const SizedBox(height: 100),
                ],
              );
            }),
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                constraints:
                    BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    top: BorderSide(width: 2, color: Colors.blue),
                  ),
                ),
                child: SizedBox(
                  height: 50,
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text("Place Order"),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _CartSummary extends StatelessWidget {
  const _CartSummary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cartBloc = BlocProvider.of<CartBloc>(context);
    final cart = cartBloc.state.items;

    return Container(
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Wrap(
        spacing: 12,
        runSpacing: 12,
        children: [
          Text(
            "PRICE DETAILS (${cartBloc.state.count} items)",
            style: const TextStyle(color: Colors.grey, fontSize: 18),
          ),
          const Divider(height: 1, color: Colors.grey),
          Row(
            children: [
              const Expanded(
                  child: Text(
                "Total Price",
                style: TextStyle(fontSize: 16),
              )),
              Text(
                "${cart.totalMRP}",
                style: const TextStyle(fontSize: 16),
              ),
            ],
          ),
          Row(
            children: [
              const Expanded(
                  child: Text(
                "Discount",
                style: TextStyle(fontSize: 16),
              )),
              Text(
                "- ${cart.discountedPrice}",
                style: const TextStyle(color: Colors.lightGreen, fontSize: 16),
              ),
            ],
          ),
          Row(
            children: [
              const Expanded(
                  child: Text(
                "Delivery Charges",
                style: TextStyle(fontSize: 16),
              )),
              cart.deliveryCharges > 0
                  ? Text(
                      "+ ${cart.deliveryCharges}",
                      style: const TextStyle(color: Colors.blue, fontSize: 16),
                    )
                  : const Text(
                      "FREE",
                      style: TextStyle(color: Colors.green, fontSize: 16),
                    ),
            ],
          ),
          const Divider(height: 1, color: Colors.grey),
          Row(
            children: [
              const Expanded(
                  child: Text(
                "Final Price",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              )),
              Text(
                "${cart.totalPrice}",
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          const Divider(height: 1, color: Colors.grey),
          Text(
            "You will save ${cart.discountedPrice} on this order.",
            style: const TextStyle(
              color: Colors.green,
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}

class _CartItem extends StatelessWidget {
  final CartItem cartItem;
  const _CartItem({Key? key, required this.cartItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cart = BlocProvider.of<CartBloc>(context);
    final product = cartItem.product;
    // const bgColor = Color(0xe0e0e0);

    return Container(
      padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Container(
        padding: const EdgeInsets.all(20),
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment(-1.0, -4.0),
                end: Alignment(1.0, 4.0),
                colors: [
                  Color(0xFFf0f0f0),
                  Color(0xFFefefef),
                ]),
            borderRadius: BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFd9d9d9),
                  offset: Offset(5.0, 5.0),
                  blurRadius: 15.0,
                  spreadRadius: 1.0),
              BoxShadow(
                  color: Color(0xFFffffff),
                  offset: Offset(-5.0, -5.0),
                  blurRadius: 15.0,
                  spreadRadius: 1.0),
            ]),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.title,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 18),
                      ),
                      Space.vSpace(2),
                      Text(
                        product.description,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style:
                            const TextStyle(fontSize: 12, color: Colors.grey),
                      ),
                      Space.vSpace(8),
                      Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                            decoration: BoxDecoration(
                              color: product.rating.ratingColor,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(50)),
                            ),
                            child: Text(
                              "${product.rating.rate} ★",
                              style: const TextStyle(
                                  fontSize: 10, color: Colors.white),
                            ),
                          ),
                          Space.hSpace(10),
                          Text(
                            "(${product.rating.count})",
                            style: const TextStyle(
                                fontSize: 12, color: Colors.grey),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Image(
                  image: NetworkImage(product.image),
                  width: 80,
                  height: 80,
                  fit: BoxFit.contain,
                )
              ],
            ),
            Space.vSpace(10),
            Row(
              children: [
/*
(product.price.mrp * cartItem.quantity).toStringAsFixed(2)
"₹ ${(product.price.price * cartItem.quantity).toStringAsFixed(2)}"
"${product.price.discount}% off"
 */
                Expanded(
                  child: ProductPriceText(
                    priceText:
                        "₹ ${(product.price.price * cartItem.quantity).toStringAsFixed(2)}  ",
                    mrpText: (product.price.mrp * cartItem.quantity)
                        .toStringAsFixed(2),
                    discountText: "  ${product.price.discount}% off",
                  ),
                ),
                ProductQuantityWidget(
                  quantity: cartItem.quantity,
                  add: () {
                    cart.add(IncreaseQuantityEvent(item: cartItem));
                  },
                  remove: () {
                    if (cartItem.quantity > 1) {
                      cart.add(DecreaseQuantityEvent(item: cartItem));
                    }
                  },
                ),
              ],
            ),
            Space.vSpace(10),
            IntrinsicHeight(
              child: Row(
                children: [
                  Expanded(
                    child: TextButton.icon(
                      onPressed: () {},
                      style: TextButton.styleFrom(primary: Colors.black87),
                      icon: const Icon(Icons.favorite_outline),
                      label: const Text("Save for later"),
                    ),
                  ),
                  const VerticalDivider(thickness: 2),
                  Expanded(
                    child: TextButton.icon(
                      onPressed: () {
                        cart.add(RemoveFromCartEvent(item: cartItem));
                      },
                      style: TextButton.styleFrom(primary: Colors.black87),
                      icon: const Icon(Icons.delete),
                      label: const Text("Remove"),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
