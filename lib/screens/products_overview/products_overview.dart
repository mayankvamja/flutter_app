import 'package:flutter/material.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/screens/product-details/product_details.dart';
import 'package:fluttery/screens/shop-home/ui/products_grid.dart';
import 'package:fluttery/widgets/cart_button.dart';
// import 'package:fluttery/widgets/app_drawer.dart';

enum ProductFilters { favorites, all }

class ProductsOverview extends StatelessWidget {
  const ProductsOverview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _products =
        ModalRoute.of(context)?.settings.arguments as List<Product>;

    return Scaffold(
      // drawer: const AppDrawer(),
      appBar: AppBar(
        title: const Text("AShop"),
        actions: const [
          CartButton(),
        ],
      ),
      body: SafeArea(child: ProductsGrid(products: _products)),
    );
  }
}
