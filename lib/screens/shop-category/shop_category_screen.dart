import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttery/screens/shop-category/bloc/shop_category_bloc.dart';
import 'package:fluttery/screens/shop-home/ui/products_grid.dart';
import 'package:fluttery/utils/blos_status.dart';
import 'package:fluttery/utils/http.dart';
import 'package:fluttery/utils/spacer.dart';
import 'package:fluttery/utils/utils.dart';

class ShopCategoryScreen extends StatelessWidget {
  const ShopCategoryScreen({
    required ShopCategoryBloc bloc,
    Key? key,
  })  : _shopCategoryBloc = bloc,
        super(key: key);

  final ShopCategoryBloc _shopCategoryBloc;

  void _loadCategoryProducts(String category) {
    _shopCategoryBloc.add(FetchCategoryProducts(
      repository: ShopRepository.shared,
      category: category,
    ));
  }

  Widget _categoryProducts(String category) {
    return BlocBuilder<ShopCategoryBloc, ShopCategoryState>(builder: (
      BuildContext context,
      ShopCategoryState state,
    ) {
      if (state.isLoading) {
        return const Center(child: CircularProgressIndicator());
      }

      final error = state.errorMessage;
      final products = state.products;

      if (error != null) {
        return Center(
          child: Column(
            children: [
              Text(error),
              ElevatedButton(
                onPressed: () {
                  _loadCategoryProducts(category);
                },
                child: const Text("Try Again"),
              )
            ],
          ),
        );
      }
      if (products != null) {
        return ProductsGrid(scrollable: false, products: products);
      }
      return Container();
    });
  }

  @override
  Widget build(BuildContext context) {
    final _argument = ModalRoute.of(context)?.settings.arguments as String?;
    final category = _argument ?? "Shop";
    _loadCategoryProducts(category);

    return Scaffold(
      appBar: AppBar(title: Text(category)),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 20),
                    decoration: BoxDecoration(
                      color: Utils.generateRandomColors(category).first,
                      shape: BoxShape.circle,
                    ),
                    child: Utils.generateRandomIcon(category),
                  ),
                  Space.hSpace(20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        category.toUpperCase(),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        overflow: TextOverflow.clip,
                        style: const TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      const Text(
                        "Lorem ipsum lorem ipsum!",
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                        ),
                      )
                    ],
                  )
                ],
              ),
              Space.vSpace(10),
              const Divider(height: 2, color: Colors.grey),
              Space.vSpace(10),
              _categoryProducts(category),
            ],
          ),
        ),
      ),
    );
  }
}
