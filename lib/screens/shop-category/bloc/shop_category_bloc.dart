import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fluttery/models/product.dart';
import 'package:fluttery/utils/blos_status.dart';
import 'package:fluttery/utils/http.dart';

part 'shop_category_event.dart';
part 'shop_category_state.dart';

class ShopCategoryBloc extends Bloc<ShopCategoryEvent, ShopCategoryState> {
  ShopCategoryBloc() : super(const ShopCategoryState()) {
    on<FetchCategoryProducts>((event, emit) async {
      emit(state.copyWith(isLoading: true, products: []));
      try {
        final products = await event.fetchProducts();
        emit(state.copyWith(
          isLoading: false,
          products: products,
        ));
      } on Exception {
        emit(state.copyWith(
          isLoading: false,
          errorMessage: "Something went wrong...",
        ));
      }
    });
  }
}
