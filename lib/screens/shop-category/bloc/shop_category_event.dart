part of 'shop_category_bloc.dart';

abstract class ShopCategoryEvent extends Equatable {
  final ShopRepository repository;
  const ShopCategoryEvent({required this.repository});

  @override
  List<Object> get props => [];
}

class FetchCategoryProducts extends ShopCategoryEvent {
  final String category;
  const FetchCategoryProducts(
      {required ShopRepository repository, required this.category})
      : super(repository: repository);

  Future<List<Product>> fetchProducts() async {
    return await repository.getCategoryProducts(category);
  }
}
