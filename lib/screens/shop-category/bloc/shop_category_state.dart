part of 'shop_category_bloc.dart';

class ShopCategoryState extends Equatable {
  final BlocStatus status;
  final bool isLoading;
  final List<Product>? products;
  final String? errorMessage;
  const ShopCategoryState({
    this.isLoading = false,
    this.status = BlocStatus.initial,
    this.products,
    this.errorMessage,
  });

  ShopCategoryState copyWith({
    bool isLoading = false,
    BlocStatus status = BlocStatus.initial,
    List<Product>? products,
    String? errorMessage,
  }) {
    return ShopCategoryState(
      isLoading: isLoading,
      status: status,
      products: products ?? this.products,
      errorMessage: errorMessage,
    );
  }

  @override
  List<Object?> get props => [products];
}
