import 'package:flutter/foundation.dart';
import 'package:fluttery/models/product.dart';

class CartItem {
  final String id;
  final Product product;
  int quantity;

  CartItem({
    required this.id,
    required this.product,
    required this.quantity,
  });

  CartItem copyWith({
    required int quantity,
  }) {
    return CartItem(id: id, product: product, quantity: quantity);
  }
}

class Cart with ChangeNotifier {
  final Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  List<CartItem> get cartItems {
    return items.values.toList();
  }

  int get count {
    return _items.values.length;
  }

  int get itemsCount {
    return _items.values.toList().itemsCount;
  }

  double get totalMRP {
    return _items.values.toList().totalMRP;
  }

  double get totalPrice {
    return _items.values.toList().totalPrice;
  }

  double get discountedPrice {
    return _items.values.toList().discountedPrice;
  }

  double get deliveryCharges {
    return _items.values.toList().deliveryCharges;
  }

  void addItem(Product product, {int quantity = 1}) {
    if (_items.containsKey(product.id)) {
      _items.update(
        "${product.id}",
        (value) => CartItem(
          id: value.id,
          product: product,
          quantity: quantity + 1,
        ),
      );
    } else {
      _items.putIfAbsent(
        "${product.id}",
        () => CartItem(
          id: DateTime.now().toString(),
          product: product,
          quantity: quantity,
        ),
      );
    }
    notifyListeners();
  }

  void increaseQuantity(String productId) {
    _items.update(
      productId,
      (value) => CartItem(
        id: value.id,
        product: value.product,
        quantity: value.quantity + 1,
      ),
    );
    notifyListeners();
  }

  void decreaseQuantity(String productId) {
    if (_items[productId]?.quantity == 1) {
      removeItem(productId);
      return;
    }

    _items.update(
      productId,
      (value) => CartItem(
        id: value.id,
        product: value.product,
        quantity: value.quantity - 1,
      ),
    );
    notifyListeners();
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }
}

extension CartItemsExtension on List<CartItem> {
  List<CartItem> addingItem(Product product, {int quantity = 1}) {
    final index = indexWhere((item) => item.product.id == product.id);
    List<CartItem> list = [...this];
    if (index == -1) {
      list.add(CartItem(
        id: DateTime.now().toString(),
        product: product,
        quantity: quantity,
      ));
    } else {
      final item = list.elementAt(index);
      list[index] = CartItem(
        id: item.id,
        product: product,
        quantity: item.quantity + quantity,
      );
    }
    return list;
  }

  List<CartItem> increasingQuantity(CartItem cartItem, {int by = 1}) {
    final index = indexOf(cartItem);
    if (index == -1) {
      // original obj
      return this;
    }

    List<CartItem> list = [...this];
    final item = list.elementAt(index);
    list[index] = CartItem(
      id: item.id,
      product: item.product,
      quantity: item.quantity + by,
    );
    // copied obj
    return list;
  }

  List<CartItem> decreasingQuantity(CartItem cartItem, {int by = 1}) {
    final index = indexOf(cartItem);
    if (index == -1) {
      // original obj
      return this;
    }

    List<CartItem> list = [...this];
    final item = list.elementAt(index);
    final newQuantity = item.quantity - by;

    if (newQuantity <= 0) {
      removeAt(index);
    } else {
      list[index] = CartItem(
        id: item.id,
        product: item.product,
        quantity: item.quantity - by,
      );
    }

    // copied obj
    return list;
  }

  int get itemsCount {
    return fold(
        0, (int previousValue, element) => previousValue + element.quantity);
  }

  double get totalMRP {
    return fold(
        0.0,
        (double previousValue, element) =>
            previousValue + element.product.price.mrp);
  }

  double get totalPrice {
    final itemsTotal = fold(
        0.0,
        (double previousValue, element) =>
            previousValue + element.product.price.price);
    return itemsTotal + deliveryCharges;
  }

  double get discountedPrice {
    return fold(
        0.0,
        (double previousValue, element) =>
            previousValue + element.product.price.discountAmount);
  }

  double get deliveryCharges {
    return (itemsCount >= 4) ? 0 : 40;
  }
}
