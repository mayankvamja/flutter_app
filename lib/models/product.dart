import 'dart:math';

import 'package:flutter/material.dart';

T cast<T>(x, T defaultValue) => x is T ? x : defaultValue;
T? tryCast<T>(x) => x is T ? x : null;

class ProductPrice {
  final double price;
  final double mrp;
  final int discount;
  final int discountAmount;

  ProductPrice({
    required this.mrp,
    required this.price,
    required this.discount,
    required this.discountAmount,
  });

  factory ProductPrice.fromValue(dynamic value) {
    double mrp = cast<num>(value, 0).toDouble();
    List<int> _list = [10, 15, 20, 25, 30];
    int _discount = (_list..shuffle()).first;
    int _discountAmount = (mrp * (_discount / 100)).toInt();
    double _priceAmount = mrp - (mrp * (_discount / 100));
    return ProductPrice(
      mrp: mrp,
      price: _priceAmount,
      discount: _discount,
      discountAmount: _discountAmount,
    );
  }
}

class ProductRating {
  final double rate;
  final int count;

  Color get ratingColor {
    if (rate < 2) {
      return Colors.red;
    } else if (rate >= 2 && rate < 4) {
      return Colors.amber;
    }
    return Colors.green;
  }

  ProductRating(this.rate, this.count);

  factory ProductRating.fromJson(Map<String, dynamic> parsedJson) {
    final rate = cast<double>(parsedJson["rate"], 0);
    final count = cast<int>(parsedJson["count"], 1);
    return ProductRating(rate, count);
  }
}

class Product with ChangeNotifier {
  final int id;
  final String title;
  final String description;
  final String category;
  final String image;
  final ProductPrice price;
  final ProductRating rating;
  bool isFavorite;

  Product({
    required this.id,
    required this.title,
    required this.description,
    required this.image,
    required this.price,
    required this.category,
    required this.rating,
    this.isFavorite = false,
  });

  void toggleFavoriteStatus() {
    isFavorite = !isFavorite;
    notifyListeners();
  }

  factory Product.fromJson(Map<String, dynamic> parsedJson) {
    Map<String, dynamic> _ratingMap = parsedJson["rating"];

    return Product(
      id: cast<int>(parsedJson["id"], Random().nextInt(1000)),
      title: cast<String>(parsedJson["title"], "-"),
      description: cast<String>(parsedJson["description"], "- - -"),
      category: cast<String>(parsedJson["category"], "-"),
      image: cast<String>(parsedJson["image"], ""),
      rating: ProductRating.fromJson(_ratingMap),
      price: ProductPrice.fromValue(parsedJson["price"]),
    );
  }
}
