import 'package:flutter/material.dart';
import 'package:fluttery/models/cart.dart';

enum OrderStatus { ordered, shipped, outForDelivery, delivered, cancelled }

class OrderItem {
  final String id;
  final List<CartItem> products;
  final DateTime dateTime;
  final OrderStatus status;

  OrderItem({
    required this.id,
    required this.products,
    required this.dateTime,
    this.status = OrderStatus.ordered,
  });
}

class Orders with ChangeNotifier {
  final List<OrderItem> _orders = [];

  List<OrderItem> get orders {
    return _orders;
  }

  void addOrder(List<CartItem> products, double total) {
    _orders.insert(
      0,
      OrderItem(
        id: DateTime.now().toString(),
        products: products,
        dateTime: DateTime.now(),
      ),
    );
  }
}
