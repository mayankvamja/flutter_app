class Routes {
  Routes._();

  static const home = "";
  static const login = "login";
  static const shop = "shop";
  static const productsOverview = "products-overview";
  static const category = "category";
  static const productDetail = "product-detail";
  static const cart = "cart";
  static const orders = "orders";
}
